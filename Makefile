VERSION=$(shell git describe --abbrev=0 --tags)$(shell \
	git diff --quiet && git describe --exact-match HEAD >/dev/null || echo -dev)

build:
	cd package/translate && ./merge && ./build
	zip -r "plasma-kickoff-grid-$(VERSION).plasmoid" package

install: build
	kpackagetool5 --install "plasma-kickoff-grid-$(VERSION).plasmoid" 2>/dev/null || kpackagetool5 --upgrade "plasma-kickoff-grid-$(VERSION).plasmoid"


.PHONY: build install
