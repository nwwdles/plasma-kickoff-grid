# Kickoff+Grid

Plasma's [Kickoff](https://userbase.kde.org/Plasma/Kickoff) with favorites in columns.

## Installation

1. Download and install the plasmoid:

   - By using "Get New Widgets..." menu.
   - Or by [downloading .plasmoid manually from KDE Store](https://store.kde.org/p/1317546/) and installing it either by dragging the file to the taskbar, or using "Add Widgets...>Get New Widgets...>Install Widget From Local File..." menus.
   - Or by cloning this repo and running
     ```sh
     zip -r "plasma-kickoff-grid.plasmoid" package
     kpackagetool5 --install "plasma-kickoff-grid.plasmoid"
     ```

2. Switch to it by right-clicking on Application Launcher > Show Alternatives... > Choose Application Launcher (Favorites in a Grid).

## Screenshots

<div style="text-align:center">

![2 columns, labels on the right](https://i.imgur.com/XO9fOor.png)
![3 columns, bottom labels](https://i.imgur.com/pvPlkxK.png)

</div>

## Thanks

- Spanish translation: Carlos Barraza
