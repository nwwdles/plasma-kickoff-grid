# Translations

- Spanish translation: Carlos Barraza

`build`, `merge` and `plasmalocaletest` by Zren (taken from [plasma-applet-eventcalendar](https://github.com/Zren/plasma-applet-eventcalendar/tree/master/package/translate))

## Translation status

|  Locale  |  Lines  | % Done|
|----------|---------|-------|
| Template |      54 |       |
| ar       |   23/54 |   42% |
| ast      |   28/54 |   51% |
| az       |   28/54 |   51% |
| bs       |   17/54 |   31% |
| ca       |   28/54 |   51% |
| ca       |   28/54 |   51% |
| cs       |   28/54 |   51% |
| da       |   28/54 |   51% |
| de       |   28/54 |   51% |
| el       |   23/54 |   42% |
| en_GB    |   28/54 |   51% |
| es       |   53/54 |   98% |
| et       |   28/54 |   51% |
| eu       |   28/54 |   51% |
| fi       |   28/54 |   51% |
| fr       |   28/54 |   51% |
| gl       |   28/54 |   51% |
| he       |   23/54 |   42% |
| hu       |   28/54 |   51% |
| ia       |   24/54 |   44% |
| id       |   28/54 |   51% |
| it       |   28/54 |   51% |
| ja       |   27/54 |   50% |
| kk       |   15/54 |   27% |
| ko       |   28/54 |   51% |
| lt       |   28/54 |   51% |
| lv       |   28/54 |   51% |
| ml       |   24/54 |   44% |
| nb       |   21/54 |   38% |
| nds      |   17/54 |   31% |
| nl       |   28/54 |   51% |
| nn       |   28/54 |   51% |
| pa       |   24/54 |   44% |
| pl       |   28/54 |   51% |
| pt_BR    |   28/54 |   51% |
| pt       |   28/54 |   51% |
| ro       |   28/54 |   51% |
| ru       |   28/54 |   51% |
| sk       |   28/54 |   51% |
| sl       |   28/54 |   51% |
| sr       |   25/54 |   46% |
| sr       |   25/54 |   46% |
| sr       |   25/54 |   46% |
| sr       |   25/54 |   46% |
| tg       |   25/54 |   46% |
| tr       |   25/54 |   46% |
| uk       |   28/54 |   51% |
| vi       |   15/54 |   27% |
| zh_CN    |   28/54 |   51% |
| zh_TW    |   28/54 |   51% |
