# TODO

- [ ] Fix wrong page sometimes being opened instead of the search page when mouse is over a button.
- [ ] Clean up keyboard controls code.
- [ ] Make sure scrolling behavior is okay (it had to be reimplemented to support horizontal scrolling without holding down alt).
- [ ] Try to submit at least a subset of changes to upstream.
  - [x] Restoring favorites order when dragging item to desktop (seem to be accepted).
  - [ ] Use model for tabs (needs refactoring).
  - [ ] Grid view.
- [ ] Seting more compact margins.
- [ ] Better grid control (rows/columns).
- [ ] Better support for other tabs, add configuration for the "arrow" item?
